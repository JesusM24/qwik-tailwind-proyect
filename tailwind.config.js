const defaultTheme = require('tailwindcss/defaultTheme')
/** @type {import('tailwindcss').Config} */
module.exports = {
content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    screens: {
      'xs': '375px',
      ...defaultTheme.screens,
    },

    extend: {
      backgroundImage: {
        'banner': "url('https://www.bugatti.com/fileadmin/_processed_/sei/p63/se-image-3134866043d56b30b2b7d02ac1445602.jpg')",
      }
    }
  },
  plugins: [],
}
