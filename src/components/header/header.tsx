import { component$, useStylesScoped$, useStore, useClientEffect$, useContext, useTask$ } from '@builder.io/qwik';
import { MyContext } from '~/root';
import Modal from '../modals/modal';

export default component$(() => {
  const store = useStore({
    scrolled: false,
    numItems: 0,
    modal: false,
    cart: []
  })

  const contextState = useContext(MyContext)

/*   useClientEffect$(() => {
    if(localStorage.getItem('car-basket')) {
      const currBasket = JSON.parse(localStorage.getItem('car-basket'))
  
      const numItemsInBasket = currBasket.items.length
      store.numItems = numItemsInBasket
      store.cart = currBasket.items
    }
  }) */

  useTask$(({track}) => {
    const tempCart = track(() => contextState.items)
    store.numItems = tempCart.length
    store.cart = tempCart
  })

  return (
    <header class={`fixed top-0 left-0 w-full flex justify-between items-center p-4 text-white xs:text-2xl sm:text-5xl sm:p-8 z-40
    ${store.scrolled ? 'bg-orange-600 bg-opacity-70 shadow' : 'bg-transparent'}`}
    document:onScroll$={() => {
      if 
      (window.scrollY > 0) {
        store.scrolled = true
      } else {
        store.scrolled = false
      }
    }}>

        <a href='/'>SuperStore</a>
        <div class="text-xl sm:text-3xl relative cursor-pointer" onClick$={() => {
          store.modal = true
        }}>
          <i class="fa-solid fa-cart-shopping"></i>
          {store.numItems > 0 && <div class="absolute -top-3 -right-3 bg-white rounded-full h-6 w-6 text-xs text-black font-bold grid place-items-center">{store.numItems}</div>}
      </div>

      {store.modal && <>
        <div id="modal" class="absolute sm:w-3/5 lg:w-2/4 top-0 right-0 w-full h-screen bg-white z-50 gap-4 p-4 pt-8 text-slate-900 overflow-scroll">
          <div class="flex items-center justify-between pb-4 border-b border-b-orange-600 pr-3">
              <h1 class="font-bold text-4xl pl-5">Cart</h1>
              <i id="modal-close" class="hover:transform hover:scale-110 hover:rotate-45 fa-solid fa-xmark cursor-pointer"
              onClick$={() => {
                store.modal = false
              }}></i>
          </div>
      
          <div class="flex flex-col">
            {store.cart.length > 0 ? store.cart.map((item, i) => {
              return(
                  <div class="bg-slate-900 flex flex-col gap-1 border-b border-b-orange-600">
                    <div class="bg-white p-4 flex items-center justify-between text-slate-900">
                      <div class="flex flex-col gap-1"> 
                        <h2 class="text-lg">{item.name}</h2> 
                        <p>{item.price}</p>
                      </div>
                      <i onClick$={() => {
                        contextState.items = contextState.items.reduce
                        ((acc, curr, index) => {
                          if (index !== i) {
                            return [...acc, curr]
                          }
                          return acc
                        }, []) 
                      }} class="fa-solid fa-xmark text-sm cursor-pointer hover:opacity-40"></i>
                    </div>
                  </div> 
                )
              })
              : <div><h3 class="text-lg">Tu carro esta vacio</h3></div>
            }
            <button class="border border-solid border-slate-800 bg-orange-600 bg-opacity-70 rounded py-4 text-xl w-3/5 mx-auto
            hover:transform hover:scale-105 hover:bg-opacity-50 mt-4">Checkout</button>
          </div>
        </div> 
      </>}
      
    </header>
  );
});
