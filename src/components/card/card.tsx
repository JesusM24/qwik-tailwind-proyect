import { component$} from '@builder.io/qwik';
import { Link } from '@builder.io/qwik-city';
import "../../styles/index.scss"


export default component$(({type, data}) => {


  return (
      <Link href="/cars">
        <div class={`flex xs:flex-col shadow-md ${type % 2 == 0 ? `lg:flex-row` : `lg:flex-row-reverse`}`} onClick$={() => {
          localStorage.setItem('cars', JSON.stringify(data))
          }}>
          <div class="flex flex-col lg:w-2/4 xl:w-7/12 2xl:w-3/5 ">
            <img src={data.url} alt="name"/>
          </div>

          <div class={`flex flex-col text-center gap-4 lg:w-2/4 xl:w-5/12 2xl:w-2/5 xs:pt-4 md:pt-1 lg:pt-3 xl:pt-8 2xl:pt-12 xs:pb-4 
            selection:text-white selection:bg-orange-600 border-2 hover:border-slate-300 ${type % 2 == 0 ? `border-l-0` : `border-r-0`}`}>
            <h1 class="xs:text-2xl lg:text-2xl xl:text-2xl 2xl:text-4xl text-orange-800 font-semibold">{data.name}</h1>
            <p class="xs:text-md md:text-md lg:text-lg xl:text-xl 2xl:text-xl xs:px-10 md:px-8 xl:px-10 xl:py-4 2xl:py-10">{data.description}</p>
            <p class="xs:text-xl lg:text-xl xl:text-3xl xl:pb-4 2xl:pb-10 font-black hover:text-orange-800">{data.price}</p>
          </div>
        </div>
      </Link>
  );
});
