import { component$} from '@builder.io/qwik';
import Card from '../card/card';

export default component$(() => {
    /*   const store = useStore({
        scrolled: false
    }) */

    let contador = 0;
    
    const supercars = [
    {name: 'McLaren 720s', 
    url:  'https://mclaren.scene7.com/is/image/mclaren/720S-Coupe_hero:crop-16x9?wid=1980&hei=1114', 
    price: '$6.200.000', 
    description: 'Superdeportivo fabricado por McLaren desde 2018. Inaugura la segunda generación de la gama Super Series de McLaren Automotive.'},

    {name: 'McLaren Artura', 
    url: 'https://www.autodata1.com/media/mclaren/pics/mclaren-artura-%5B34262%5D.jpg', 
    price: '$275.000', 
    description: 'El Artura es el primer plug-in hybrid de la marca, que estrena un sistema híbrido muy ligero que se basa en un motor V6 más compacto instalado longitudinalmente en el centro del auto.'},
    
    {name: 'Mercedes AMG GTR Pro', 
    url: 'https://1.bp.blogspot.com/-j4awTdvnZro/XU0mjx4F8lI/AAAAAAAAWMU/PO_7PvjKSXQjGQfM_AoWMkQ01j7DYtbygCLcBGAs/s1600/Mercedes-AMG-GT-R-PRO%2B%252811%2529.jpg', 
    price: '$200.000', 
    description: 'El Mercedes-AMG GT es un automóvil deportivo Gran Turismo de 2 puertas biplaza, con motor central-delantero montado longitudinalmente y de propulsión trasera o total'},
    
    {name: 'Ferrari F8 Tributo', 
    url: 'https://www.autonocion.com/wp-content/uploads/2022/07/Ferrari-F8-Tributo-de-Zacoe-1-1130x706.jpg', 
    price: '$275.000', 
    description: 'El Ferrari F8 Tributo es la nueva berlinetta biplaza con motor central-trasero, máximo exponente de este tipo de vehículo de la casa del Cavallino Rampante.'},
    
    {name: 'Aston Martin – Vantage F1 Edition', 
    url: 'https://www.autocar.co.uk/sites/autocar.co.uk/files/images/car-reviews/first-drives/legacy/1-aston-martin-f1-edition-2021-uk-fd-hero-front.jpg', 
    price: '$200.000', 
    description: 'Basado en el Safety Car Oficial de la Fórmula 1, el Aston Martin Vantage F1 Edition 2022, celebra el regreso de la marca a la Fórmula 1 por primera vez en más de 60 años, estableciendo al Vantage como un super deportivo.'},
    
    {name: 'Mercedes AMG GT Black Series', 
    url: 'https://static.motor.es/fotos-noticias/2020/07/mercedes-amg-gt-black-series-2021-202069154-1594802419_1.jpg', 
    price: '$450.000', 
    description: 'El Mercedes-AMG GT Black Series es un superdeportivo de 730 caballos de potencia. Se presentó en julio de 2020 y estuvo en producción hasta enero de 2022'},
    ]

    return (
        <div class="container mx-auto"> 
            <div id='products' class="min-h-screen grid ">
                <div class="grid grid-rows-1 gap-10">
                    {supercars.map((el) => {
                        const val = contador;
                        contador++;
                        return <Card type={val} data={el} key={val}/>
                    })}
                </div>
            </div>
        </div>
    );
});