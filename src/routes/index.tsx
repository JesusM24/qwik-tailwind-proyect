import { component$ , useContext, useClientEffect$ } from '@builder.io/qwik';
import type { DocumentHead } from '@builder.io/qwik-city';
import ProductsGrid from '~/components/products-grid/products-grid';
import { MyContext } from '~/root';



export default component$(() => {

  const contextState = useContext(MyContext)

  useClientEffect$(() => {
    if (localStorage.getItem('car-basket')){
      contextState.items = [...JSON.parse(localStorage.getItem('car-basket')).items]
    }
  })

  return (
    <div class="flex flex-1 flex-col w-full">

      <div class=" absulote bg-banner bg-no-repeat h-screen bg-fixed bg-full bg-cover bg-center mb-4">
        <section class="min-h-screen flex relative">
          <div class="container h-full">
              <a href="#products" class="absolute left-1/2 top-2/3 -translate-x-1/2 -translate-y-1/2 p-4 px-8
              border-2 text-orange-600 hover:text-white border-solid border-orange-800 text-xl font-extrabold after:absolute after:bg-orange-600
              after:right-full after:top-0 after:w-full after:h-full hover:after:translate-x-full after:duration-300
              overflow-hidden after:bg-opacity-80">
                <h3 class="relative z-20 text-center">Comprar SuperAutos</h3>
              </a>
          </div>
        </section>
      </div>
      <ProductsGrid/>
    </div>
  );
});

export const head: DocumentHead = {
  title: 'qwik-tailwind proyect',
  meta: [
    {
      name: 'description',
      content: 'Qwik site description',
    },
  ],
};
