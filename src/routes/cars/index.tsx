import { component$, useClientEffect$, useContext, useStore} from '@builder.io/qwik';
import { DocumentHead, useLocation } from '@builder.io/qwik-city';
import { MyContext } from '~/root';



export default component$(() => {

  const loc = useLocation();

  const state = useStore({
    url: '',
    name: '',
    price: '',
    description: ''
  });

  const contextState = useContext(MyContext)

  useClientEffect$(() => {
    const data = JSON.parse(localStorage.getItem('cars'))

    state.name = data.name
    state.url = data.url
    state.price = data.price
    state.description = data.description

  });

  useClientEffect$(() => {
    if (localStorage.getItem('car-basket')){
      contextState.items = [...JSON.parse(localStorage.getItem('car-basket')).items]
    }
  })

  return (
    <div class="flex flex-col gap-2">
      <img src={state.url} alt={state.name} class="object-cover relative z-10"/>
      <div class="flex flex-col gap-4 text-center">
        <h2 class="text-6xl font-bold text-orange-800 pt-8">{state.name}</h2>
        <p class="text-2xl w-2/4 mx-auto">{state.description}</p>
        <h2 class="text-2xl font-bold hover:text-orange-600 w-fit mx-auto">{state.price}</h2>
      </div>

      <button class="border-2 py-4 border-slate-900 border-solid w-fit mx-auto py-2 px-10 rounded font-bold
      hover:bg-orange-600 hover:bg-opacity-80 hover:text-white" onClick$={() => {
        let currBasket = { items: [] }
        if (localStorage.getItem('car-basket')) {
          currBasket = JSON.parse(localStorage.getItem('car-basket'))
        }
        currBasket.items.push([state])
        localStorage.setItem('car-basket', JSON.stringify(currBasket))
        contextState.items = [...contextState.items, state]
        
      }}>Comprar</button>
    </div>
  );
});

export const head: DocumentHead = {
  title: "Description",
};
